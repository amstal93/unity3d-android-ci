# unity3d-android-ci

GitLab CI image for building Unity3d Android apps

Source code is [gitlab.com/oriontvv/unity3d-android-ci](https://gitlab.com/oriontvv/unity3d-android-ci).

Example unity3d project is [gitlab.com/oriontvv/unity3d-gitlab-ci-example](https://gitlab.com/oriontvv/unity3d-gitlab-ci-example).
